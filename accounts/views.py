from django.shortcuts import render, redirect
from .forms import LoginForm, SignupForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .models import DungeonMaster, Player


# Create your views here.
def user_login(request):
    if request.method != "POST":
        form = LoginForm()
    else:
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            password_confirmation = request.POST['password_confirmation']
            if password == password_confirmation:
                user = authenticate(
                    request,
                    username=username,
                    password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("homepage")
            else:
                form.add_error("password", "The passwords do not match")
    context = {
        "form": form
    }
    return render(request, 'accounts/login.html', context)


def user_logout(request):
    logout(request)
    return redirect("homepage")


def user_signup(request):
    if request.method != "POST":
        form = SignupForm()
    else:
        form = SignupForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            role = form.cleaned_data['role']
            if password == password_confirmation:
                new_user = User.objects.create_user(
                    first_name=first_name,
                    last_name=last_name,
                    email=email,
                    username=username,
                    password=password,
                )
                login(request, new_user)
                if role == "Player":
                    player = Player.objects.create(user=new_user)
                    player.save()
                else:
                    dm = DungeonMaster.objects.create(user=new_user)
                    dm.save()
                return redirect("homepage")
            else:
                form.add_error("password", "The passwords do not match")
    context = {
        "form": form
    }
    return render(request, 'accounts/signup.html', context)
