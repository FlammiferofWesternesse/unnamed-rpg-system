from django.db import models
from django.contrib.auth.models import User


# # Create your models here.
class DungeonMaster(models.Model):
    user = models.OneToOneField(
        User,
        related_name="dungeonmasters",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name


class Player(models.Model):
    user = models.OneToOneField(
        User,
        related_name="players",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name
