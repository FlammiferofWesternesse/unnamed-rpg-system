from django import forms


role_choices = (("Player", "Player"), ("DungeonMaster", "DM"))


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=50, widget=forms.PasswordInput
        )


class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    email = forms.EmailField()
    username = forms.CharField(max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=50, widget=forms.PasswordInput
        )
    role = forms.ChoiceField(choices=role_choices)
