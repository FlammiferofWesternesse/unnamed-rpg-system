from django.contrib import admin
from .models import DungeonMaster, Player


# Register your models here.
@admin.register(DungeonMaster)
class DungeonMasterAdmin(admin.ModelAdmin):
    list_display = ["user"]


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ["user"]
