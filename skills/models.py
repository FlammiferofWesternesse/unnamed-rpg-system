from django.db import models


# Create your models here.
class Attribute(models.Model):
    name = models.CharField(max_length=12)

    def __str__(self):
        return self.name


class Branch(models.Model):
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=30)
    branch = models.ForeignKey(
        Branch,
        related_name="skills",
        on_delete=models.CASCADE
    )
    governing_attribute = models.ForeignKey(
        Attribute,
        related_name="skills",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    class Meta:
        ordering = ["branch", "name"]

    def __str__(self):
        return self.name
