# Generated by Django 4.2.2 on 2023-06-15 18:52

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("skills", "0002_branch_alter_skill_branch"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="skill",
            name="level",
        ),
    ]
