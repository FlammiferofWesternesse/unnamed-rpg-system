from django.contrib import admin
from .models import (
    Attribute,
    Branch,
    Skill,
)


# Register your models here.
@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "branch",
    ]


@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
    list_display = [
        "name"
    ]


@admin.register(Attribute)
class AttributeAdmin(admin.ModelAdmin):
    list_display = [
        "name"
    ]
