from django.contrib import admin
from .models import (
    Character,
    Path,
    Race,
    Sign
    )


# Register your models here.
@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    list_display = []


@admin.register(Path)
class PathAdmin(admin.ModelAdmin):
    list_display = [
        "path_name",
        "focus_branch",

    ]


@admin.register(Race)
class RaceAdmin(admin.ModelAdmin):
    list_display = [
        "race",
        "sex"
    ]


@admin.register(Sign)
class SignAdmin(admin.ModelAdmin):
    list_display = []
