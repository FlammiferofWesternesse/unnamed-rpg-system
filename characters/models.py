from django.db import models
from skills.models import Skill, Branch, Attribute
from django.contrib.auth.models import User


# # Create your models here.
class Path(models.Model):
    path_name = models.CharField(max_length=15)
    major_skills = models.ManyToManyField(
        Skill,
        related_name="major_skills",
        blank=True
    )
    minor_skills = models.ManyToManyField(
        Skill,
        related_name="minor_skills",
        blank=True
    )
    focused_attributes = models.ManyToManyField(
        Attribute,
        related_name="paths",
        blank=True
    )
    focus_branch = models.ForeignKey(
        Branch,
        related_name="paths",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.path_name


class Character(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    path = models.ForeignKey(
        Path, related_name="characters", on_delete=models.CASCADE
    )
    level = models.SmallIntegerField(default=1)
    owner = models.ForeignKey(
        User,
        related_name="characters",
        on_delete=models.CASCADE,
    )
    race = models.ForeignKey(
        "Race",
        on_delete=models.CASCADE,
        related_name="characters",
        blank=True,
        null=True
    )
    sign = models.ForeignKey(
        "Sign",
        on_delete=models.CASCADE,
        related_name="characters",
        blank=True,
        null=True
    )

    # Unbound Attributes and Skills
    strength = models.SmallIntegerField(default=40)
    endurance = models.SmallIntegerField(default=40)
    intelligence = models.SmallIntegerField(default=40)
    willpower = models.SmallIntegerField(default=40)
    agility = models.SmallIntegerField(default=40)
    speed = models.SmallIntegerField(default=40)
    personality = models.SmallIntegerField(default=40)
    luck = models.SmallIntegerField(default=40)
    acrobatics = models.SmallIntegerField(default=5)
    alchemy = models.SmallIntegerField(default=5)
    alteration = models.SmallIntegerField(default=5)
    armorer = models.SmallIntegerField(default=5)
    athletics = models.SmallIntegerField(default=5)
    axe = models.SmallIntegerField(default=5)
    block = models.SmallIntegerField(default=5)
    blunt_Weapon = models.SmallIntegerField(default=5)
    conjuration = models.SmallIntegerField(default=5)
    destruction = models.SmallIntegerField(default=5)
    enchant = models.SmallIntegerField(default=5)
    hand_to_hand = models.SmallIntegerField(default=5)
    heavy_armor = models.SmallIntegerField(default=5)
    illusion = models.SmallIntegerField(default=5)
    light_armor = models.SmallIntegerField(default=5)
    long_blade = models.SmallIntegerField(default=5)
    marksman = models.SmallIntegerField(default=5)
    medium_armor = models.SmallIntegerField(default=5)
    mercantile = models.SmallIntegerField(default=5)
    mysticism = models.SmallIntegerField(default=5)
    restoration = models.SmallIntegerField(default=5)
    security = models.SmallIntegerField(default=5)
    short_blade = models.SmallIntegerField(default=5)
    sneak = models.SmallIntegerField(default=5)
    spear = models.SmallIntegerField(default=5)
    speechcraft = models.SmallIntegerField(default=5)
    unarmored = models.SmallIntegerField(default=5)

    # Methods for setting max stamina/health/magicka
    def get_health_max(self):
        base_health = sum((self.strength, self.endurance))
        level_health = int(self.endurance / 10 * (self.level - 1))
        return base_health + level_health

    def get_stamina_max(self):
        return sum((
            self.strength,
            self.endurance,
            self.willpower,
            self.agility
            ))

    def get_magicka_max(self):
        return self.intelligence * self.race.magicka_multiplier

    def __str__(self):
        return self.name


class Race(models.Model):
    race = models.CharField(max_length=35)
    tier_one_skill = models.ForeignKey(
        Skill,
        related_name="tier_one_skill",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    tier_two_skills = models.ManyToManyField(
        Skill,
        related_name="tier_two_skills",
        blank=True,
    )
    tier_three_skills = models.ManyToManyField(
        Skill,
        related_name="tier_three_skills",
        blank=True,
    )

    M = "male"
    F = "female"
    sex_options = [
        (M, "Male"),
        (F, "Female")
    ]

    sex = models.CharField(
        max_length=6,
        choices=(sex_options)
    )

    strength_bonus = models.SmallIntegerField()
    intelligence_bonus = models.SmallIntegerField()
    willpower_bonus = models.SmallIntegerField()
    agility_bonus = models.SmallIntegerField()
    speed_bonus = models.SmallIntegerField()
    endurance_bonus = models.SmallIntegerField()
    personality_bonus = models.SmallIntegerField()

    magicka_multiplier = models.FloatField()

    def __str__(self):
        return self.sex.title() + " " + self.race.title()


class Sign(models.Model):
    name = models.CharField
